-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 24, 2020 at 02:08 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.2.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sqltasks`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `cid` bigint(8) NOT NULL,
  `post` bigint(8) NOT NULL,
  `user` bigint(8) NOT NULL,
  `comment` varchar(20000) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`cid`, `post`, `user`, `comment`) VALUES
(1, 12, 1, 'But testing is pretty useful though'),
(2, 44, 43, 'Modi ut et soluta deserunt. Saepe qui nesciunt illum quis in est. Quia dignissimos tenetur nam accusantium accusantium vitae libero.'),
(3, 58, 71, 'Repudiandae officia tempora dolore illum. Quis perspiciatis fugiat maiores inventore. Ut sit et velit debitis doloribus error. Culpa reiciendis soluta dolores libero fugit explicabo.'),
(4, 49, 91, 'Temporibus aut quibusdam nihil qui. Ipsum minima dolor facilis vel cupiditate. Consequatur ut hic dolorem minus repellendus.'),
(5, 43, 1, 'Id dolore ex et voluptatem id. Numquam molestiae eligendi voluptate. Suscipit vitae magni numquam corrupti. Sit voluptate eum dolorum tempore. Laudantium voluptas amet omnis est.'),
(6, 51, 34, 'Quo ut dolorem autem nihil esse. Est voluptatibus odio a aliquam. Et inventore quis tenetur eaque adipisci qui. Esse voluptates culpa placeat corporis enim dolorum.'),
(7, 28, 62, 'Magnam nihil qui in quam velit aliquid harum. Impedit excepturi molestias at eum illum. Tempore minus voluptatibus nostrum.'),
(8, 47, 52, 'Quas in sit ab tempora aperiam. Aut vel ut qui. Amet est voluptatem laborum fugit atque enim eum eaque.'),
(9, 23, 35, 'Eveniet voluptatem blanditiis nam facere provident aut. Quis asperiores quae voluptates sunt. Voluptas quibusdam repellendus vel. Suscipit ab ipsum quia necessitatibus ipsum corporis.'),
(10, 23, 100, 'Quia nostrum sit ab incidunt et ullam cum rerum. Consequatur rerum nobis esse ut omnis asperiores. Aperiam eum corporis facere sunt rerum.'),
(11, 17, 94, 'Autem ullam sunt consectetur illo sequi. Perspiciatis est nostrum aliquam cum impedit natus.'),
(12, 48, 76, 'Maiores laborum totam fugit asperiores sed dolore corporis. Unde deserunt iusto doloribus labore et et.'),
(13, 7, 6, 'Maxime repellat voluptatum voluptate nostrum cupiditate. Quisquam fugit aut iure voluptatem. Ullam error cum sit voluptate tempora asperiores. At voluptatem tempora quo quos. Aut ut harum est.'),
(14, 54, 88, 'Dicta expedita doloremque quia rerum distinctio. Aut voluptas enim adipisci est non repudiandae. Officiis ipsa architecto facere eius corporis quaerat.'),
(15, 21, 19, 'Excepturi a dolor doloremque eveniet adipisci placeat consequatur sequi. Et eaque qui aut minima voluptate et est. Saepe quisquam excepturi ex asperiores harum minima.'),
(16, 52, 71, 'Et ullam inventore autem quia non repellat. Sed laudantium facere rerum natus et voluptatem consectetur. Atque deleniti sapiente hic natus ut beatae magnam.'),
(17, 55, 48, 'Eligendi placeat commodi id vel dolore veniam. At alias ut exercitationem sed. Quo ut consequatur accusamus reprehenderit. Consequuntur suscipit doloribus accusantium illum sint.'),
(18, 47, 44, 'Libero sequi libero omnis. Et corrupti quis itaque rerum sequi molestiae. Molestias laudantium et nam inventore ut autem quo quia. Ex eveniet eos consequatur eligendi eligendi.'),
(19, 25, 14, 'Aliquid aliquid dolore incidunt dicta et. Sunt provident et reprehenderit est minima.'),
(20, 34, 50, 'Iste dolore perferendis culpa sint consequatur omnis soluta enim. Qui voluptatem ipsam deserunt voluptates molestias totam a. Inventore ea sequi id sed cumque aut fugiat.'),
(21, 5, 11, 'Dolorem saepe ut nesciunt sunt consequuntur cupiditate porro cupiditate. Eum non molestias ipsam qui possimus veritatis. Vel quas alias inventore quis ea eos.'),
(22, 38, 60, 'Quia enim debitis quas porro ex sed saepe esse. Quia ullam repellendus et sint. Accusamus unde voluptatem ducimus.'),
(23, 61, 50, 'Quis voluptatem ea ratione veniam adipisci. Placeat occaecati qui animi soluta doloribus eum rem. Voluptate fugit nulla voluptas id.'),
(24, 29, 52, 'Ex excepturi eius qui eum molestiae enim ut. Ut ut asperiores eum recusandae ex. In molestiae inventore omnis a incidunt molestiae quam. Sunt dignissimos id doloribus nostrum dolor nobis ipsam eos.'),
(25, 48, 96, 'Voluptas harum minima quia. Cupiditate error ex iusto ab et veritatis. Nisi aliquid eos mollitia voluptatem. Mollitia consequatur provident cum odio nesciunt nostrum.'),
(26, 10, 75, 'Minima deleniti aut consequatur voluptatem eum voluptatem voluptatem praesentium. Hic non enim quaerat cumque ipsum perferendis amet. Ipsa consequatur ipsum minus id. Rem tenetur ipsam ea.'),
(27, 15, 32, 'Eos repudiandae modi sit molestias occaecati. Cumque est consequuntur iste a sint sunt quod. Est quo ut odio cupiditate autem alias rerum. Nobis autem rerum quae sint possimus.'),
(28, 21, 13, 'Aut sapiente alias nisi et sunt in fugit. In voluptatibus temporibus aut.\nRepellat quidem occaecati fugiat sed. Suscipit et amet odio vero quos. Dolore quo voluptatum incidunt sed totam ut.'),
(29, 20, 3, 'Quaerat minus id accusamus error. Nisi rem architecto voluptate inventore harum doloribus maxime. Magnam laudantium expedita et adipisci.'),
(30, 13, 16, 'Omnis doloribus et nam voluptatum deserunt praesentium autem quod. Error optio sed facere eaque illum veritatis et tempore. Voluptatem commodi et aliquid. Deserunt numquam explicabo nobis aliquam.'),
(31, 4, 53, 'Debitis et consectetur illo enim enim ut eos inventore. Omnis architecto mollitia facere qui quo cupiditate.'),
(32, 9, 6, 'Deserunt eos unde nesciunt enim. Recusandae aut quisquam quod sit et. Et labore quasi sed doloribus reprehenderit facilis. Nisi velit sit sed sunt voluptas laborum omnis necessitatibus.'),
(33, 56, 33, 'Aperiam blanditiis aperiam assumenda eligendi minus sit. Sint aut error dolore doloribus quam itaque dolor. Sequi dolorem accusamus id harum. Quisquam ut illo incidunt aut facere repellat.'),
(34, 12, 84, 'Quo ut dolores in nemo. Consequatur repellendus qui enim mollitia saepe. Voluptatem iusto autem veniam rerum sed.'),
(35, 63, 57, 'Non consequatur voluptas et aut. Nihil provident voluptatum dicta. Non aut similique ut ut et aut. Voluptatem vel non iure porro ut aut illum. Illo quia ut est velit quisquam est.'),
(36, 2, 84, 'Voluptatem molestiae vel non. Cupiditate beatae similique amet quisquam. Eum in non nulla consequatur.'),
(37, 51, 41, 'Nulla voluptas distinctio maxime reiciendis ut nihil. Porro omnis et doloribus id. Aperiam magni nemo repudiandae et natus quia.'),
(38, 10, 40, 'Est aut reiciendis optio enim voluptatibus numquam. Dolor quia ipsum molestiae nobis. Vel voluptate nihil rem repudiandae.'),
(39, 67, 44, 'Eos culpa nostrum sed. Et quas qui similique suscipit rerum. Porro dolor fugiat quia dolores hic aut.'),
(40, 36, 90, 'Quod numquam magni et nisi iusto rem sit. Qui in rerum non odio commodi et. Quo voluptatem facere eaque.\nDolorum voluptate voluptates minus optio culpa. Omnis quia corporis praesentium autem laborum.'),
(41, 63, 14, 'Unde quia repellendus dicta id. Error ipsam sed possimus autem. Voluptatibus aut dolores aut a debitis quis molestiae. Exercitationem accusantium sed magni quia dolores totam harum aut.'),
(42, 69, 51, 'Laboriosam aut rerum dolor ut eius qui ratione porro. Tempore neque quasi voluptas. Maxime dignissimos explicabo in adipisci est voluptas. Repellendus nam quia nisi saepe.'),
(43, 5, 33, 'Sed aut est numquam earum nulla. Quis maxime aut expedita repellat qui eius. Et voluptatem ut ipsam est aspernatur et.'),
(44, 8, 65, 'Eos et dolorem provident voluptas voluptatem error quis. Ratione sit cupiditate numquam. Cum qui dolore molestias laudantium.'),
(45, 48, 41, 'Iste et qui non harum. Corporis tempora aut vel eligendi cumque.\nEst voluptatem enim aut a dolor. Unde odio et dolor ut voluptatem voluptatem exercitationem.'),
(46, 53, 92, 'Qui ratione et assumenda et sit. Et voluptates asperiores alias voluptatem tenetur explicabo. Voluptatibus aut eligendi odit iste.'),
(47, 47, 43, 'Quae autem ipsum voluptatem. Laborum minima corrupti fugit perspiciatis. Consequuntur illum sed dolorem cupiditate dignissimos maxime delectus ut.'),
(48, 36, 23, 'Omnis officia eius similique. Consequatur vitae adipisci cum quasi. Ut perferendis molestiae ducimus illum dignissimos omnis nihil. Ut magnam eos voluptate iste perferendis dolorem ab.'),
(49, 54, 22, 'Nam accusantium aut iste eos quasi corrupti. Quis alias consequatur unde quasi aut explicabo optio aliquid.'),
(50, 67, 52, 'Veniam suscipit voluptatem dolor. Cum omnis ipsum sint optio quisquam eligendi. Sint reiciendis veritatis sunt similique occaecati quo autem.'),
(51, 58, 30, 'Perspiciatis corporis deleniti qui aut qui et. Delectus adipisci ut quas et libero. Quisquam non impedit non odit delectus. Soluta minima non commodi quis est.'),
(52, 59, 51, 'Nihil deserunt iusto amet saepe nesciunt magnam fugiat assumenda. Deleniti odit dolor asperiores commodi est. Aut eos aperiam velit hic laboriosam. Blanditiis repellat excepturi neque corporis nobis.'),
(53, 45, 8, 'Numquam quia eaque et. Aut laborum corporis nihil animi quo maiores.'),
(54, 56, 4, 'Provident voluptas excepturi nihil et ut dolorum sit. Voluptas autem ducimus cumque earum sint laboriosam est et. Fugiat non expedita ut sed.'),
(55, 17, 23, 'Assumenda quas quaerat a voluptas et cumque incidunt. Molestiae tenetur eius explicabo. Voluptas qui voluptatem nam voluptatem et reiciendis. Et voluptas occaecati temporibus sed sit.'),
(56, 45, 70, 'At vel voluptatum sed et deleniti ratione nisi quae. Expedita perferendis praesentium quasi labore non animi.'),
(57, 25, 75, 'Numquam molestiae illo doloribus voluptatem voluptatem eum animi. Laborum occaecati voluptatum quo maxime in.'),
(58, 29, 66, 'Sit ad molestias saepe. Porro ipsam magni quaerat laborum ratione eligendi adipisci et. Ea possimus eum recusandae reiciendis minima.'),
(59, 48, 33, 'Provident sint cumque autem praesentium id quis illo. Quam eum quia aspernatur unde. Non quisquam voluptatum fugit ut et eum. Iusto eius dolore optio eius debitis quidem.'),
(60, 58, 33, 'Modi laudantium qui quaerat doloribus alias. Eum laborum atque quis quo aut. Et nihil non blanditiis dolore vel ducimus quasi. Sit debitis dicta pariatur ut consequuntur sit quae.'),
(61, 60, 29, 'Harum et recusandae et non sint. Est necessitatibus officia explicabo aut quod est. Provident vel odio veritatis nam perferendis alias. Autem perferendis praesentium cumque dignissimos omnis iure.'),
(62, 56, 51, 'Et eos maiores est fugiat fuga. Et in esse ratione. Sint eos sit provident consequatur consequatur. Consequatur debitis quia facilis quia non.'),
(63, 69, 34, 'Laborum voluptatem molestias neque quasi. Nemo dignissimos consectetur quod est quam aut. Veniam corrupti quibusdam error nisi est.'),
(64, 4, 1, 'Veritatis reiciendis sit sed quo est. Molestiae quia omnis cupiditate consequatur placeat sequi culpa. Ullam molestiae aut qui. Consequatur et quis non excepturi voluptatem.'),
(65, 46, 55, 'Animi rerum consequatur autem ut explicabo voluptas autem. Asperiores inventore veritatis hic vitae. Nisi nobis provident labore a voluptatem.'),
(66, 53, 97, 'Nihil qui quasi sed consectetur. In aut earum dolorum error fuga. Suscipit vel aliquam quidem molestias ut aut repellendus.'),
(67, 2, 62, 'Laborum voluptas earum voluptatem architecto eos quaerat. Vero omnis deserunt eos atque tenetur rem ex laborum. Tenetur atque facilis culpa accusamus.'),
(68, 1, 29, 'Velit occaecati dolore est non nesciunt exercitationem. Praesentium voluptas et aut et. Iusto est ut quae. Sit et facilis et quia dolores nihil dolorem.'),
(69, 47, 7, 'Ipsum cupiditate beatae eligendi consequatur. Quo voluptas qui delectus alias dolore nulla incidunt. Et maxime dolor sit sint hic enim qui.'),
(70, 48, 85, 'Optio rerum est unde facere vel quam in. Expedita quia accusantium sed aperiam et minus unde quasi. Impedit occaecati autem dolore illum. Provident praesentium porro iusto unde et rerum voluptatem.'),
(71, 53, 15, 'Dicta aut fuga voluptatem. Aspernatur quod sint et ducimus.\nQuo odit sed dolorum consequuntur ipsum. Est expedita praesentium et distinctio.'),
(72, 65, 21, 'Provident sed cumque quibusdam. Mollitia ea nam qui. Eius quod impedit nobis vitae consequatur eos.'),
(73, 3, 38, 'Rerum et iure cupiditate. Molestias nihil neque fugit et. Quidem reiciendis blanditiis vero pariatur et voluptas. Deserunt unde aut exercitationem et laborum.'),
(74, 56, 3, 'Beatae sed eius eaque minus illo. Et est autem nihil.'),
(75, 55, 10, 'Aut praesentium omnis delectus odit. Quis aut beatae voluptatem earum magnam ea aut. Dolorum rem consequuntur eos iste nemo est. Unde culpa sequi cum explicabo optio.'),
(76, 48, 55, 'Vel aut reiciendis id necessitatibus perspiciatis unde. Rem quas harum mollitia aut voluptas praesentium. Voluptas nemo vero tempore ut eligendi est. Est adipisci molestiae maxime natus.'),
(77, 61, 68, 'Qui ipsum et quia recusandae voluptatum qui. Et dicta qui sed est praesentium. Maxime maxime adipisci ipsam vel.'),
(78, 17, 13, 'Deleniti necessitatibus recusandae sed dolorem qui earum incidunt. Quia perferendis deleniti nihil aut et aut deserunt. Natus ratione minima voluptatum.'),
(79, 5, 68, 'Omnis delectus maxime praesentium et. Ut perferendis eaque dolorem. Optio eos dolorum dignissimos ut alias consequatur voluptas nemo.'),
(80, 15, 49, 'Consequatur rem et et sapiente. Non et eveniet voluptas in quas omnis. Et possimus qui sed velit nihil ipsum ut illum.'),
(81, 4, 72, 'Ea et nisi excepturi atque perspiciatis. Rerum ullam enim nemo ut inventore aspernatur. Sint laudantium impedit blanditiis quas eum. Hic at eveniet autem nobis ut.'),
(82, 33, 6, 'Dicta dolor earum laboriosam. Et voluptas sit consequatur deserunt. Blanditiis id et autem dicta aut tempore omnis et. Sed sit voluptas omnis voluptates quibusdam velit.'),
(83, 44, 48, 'Dolorum quia fuga quibusdam fugiat esse. Quia ex officia beatae omnis harum est laborum vitae.'),
(84, 65, 30, 'Dicta quia commodi non nulla incidunt vitae eveniet possimus. Modi optio eos sint totam ut velit omnis. Nam fuga quo veritatis qui pariatur. Similique et autem odit in.'),
(85, 32, 88, 'Omnis voluptatem incidunt officiis illo culpa vero. Possimus eos expedita sed minima. Quisquam et et ut est tenetur. Ex quisquam nobis rem quidem soluta nisi.'),
(86, 57, 65, 'Velit placeat sequi consequatur qui est. Corporis ut dolor quis sed est. Quaerat quia nostrum ipsam consequatur quia.'),
(87, 55, 40, 'Velit qui sunt voluptatem excepturi odit. Quis dolores voluptas maiores enim ut et rerum. Qui cum architecto atque est ut eaque.'),
(88, 11, 52, 'Vel cupiditate est qui cum sapiente ex. Commodi sed adipisci ipsa inventore aut tempora. Enim praesentium amet qui et et libero a. Ea libero voluptatibus iusto assumenda.'),
(89, 18, 12, 'Vel quam non consectetur excepturi veritatis perferendis odit. Id voluptas repudiandae non impedit quasi impedit nihil et. Repellat dignissimos molestiae id voluptas.'),
(90, 16, 31, 'Nesciunt laudantium libero voluptatum nihil beatae non. Et eos eius sed quae animi quia dolores quis. Et numquam possimus sapiente incidunt modi fuga.'),
(91, 48, 64, 'Nulla molestiae perferendis itaque asperiores. Nobis cumque sed earum. Aut et voluptatem et non aut.'),
(92, 14, 7, 'Asperiores sed corrupti iure dolorem quidem. Ut dolore modi eos blanditiis quia laudantium. Incidunt itaque et ullam qui voluptatem molestiae.'),
(93, 54, 63, 'Voluptatum quis fugit ipsum. Error quia dolorum eaque necessitatibus perspiciatis explicabo. Tempora sit ut voluptates iste ut voluptatem et.'),
(94, 40, 54, 'Voluptate et voluptas quibusdam. Voluptas tempora laudantium laudantium et facilis deserunt. Facilis facere dignissimos et fuga.'),
(95, 48, 49, 'Suscipit sit commodi deleniti aspernatur. Quisquam aliquid quos sed pariatur. Quia autem voluptates sint quidem et possimus.\nAd quidem quisquam est id similique. Facere qui non quas voluptas aut.'),
(96, 33, 19, 'Velit totam illum modi rem qui. Aut et quaerat vero qui. Perferendis non fuga quibusdam.'),
(97, 63, 30, 'Voluptas maiores quaerat expedita laudantium et sed labore. Ipsum vel aut sed nulla quidem. Quod sit veniam ipsa voluptas voluptate nihil inventore ut.'),
(98, 46, 89, 'Temporibus ullam iure quasi. Animi accusamus qui quidem nam blanditiis. Cum dolorem qui sequi ut qui ut cum. Velit veniam laudantium nihil eligendi eius fuga accusamus.'),
(99, 58, 20, 'Architecto ipsam quo alias et. Aut nesciunt minima facere. Eius quaerat vel eveniet id architecto quia.'),
(100, 56, 95, 'Id nam laboriosam quidem consequatur. Est corrupti qui eius. Sit nisi et eum veritatis. Velit labore qui quidem voluptatem deleniti laudantium.'),
(101, 15, 61, 'Voluptatem aut ex harum et dolor. Minima est sed aliquam dolor ipsam. Eligendi perspiciatis qui illo soluta voluptas a.'),
(102, 51, 4, 'Dolores totam veritatis autem eius illum et. In odit omnis omnis commodi quasi. Aliquid tempora rem ea amet qui.'),
(103, 13, 89, 'Qui blanditiis quod consequatur soluta fugit itaque iure. Perferendis debitis autem nobis et non eos. Tempora tempora et aut. Iure dignissimos repudiandae qui ut.'),
(104, 59, 72, 'Assumenda sunt numquam et omnis quaerat ullam cupiditate. Incidunt est doloribus officia non. Necessitatibus et dolorum sapiente eius. Recusandae quas praesentium magnam ad eos sapiente et enim.'),
(105, 64, 78, 'Architecto cupiditate sed placeat sint mollitia. Et laborum sit blanditiis et aspernatur eveniet iste delectus. Consequatur dolorum tempora nemo non iure debitis. Placeat consequatur earum sed autem.'),
(106, 28, 88, 'Nostrum pariatur ut dolores aut consequatur est. Vel error vero fugit cumque molestiae omnis.\nAdipisci praesentium nobis sint quo et. Et vel voluptatum quia tempora perferendis non reprehenderit eum.'),
(107, 16, 53, 'Repellat ut consequuntur vitae eos at. Voluptatibus non possimus officiis quaerat cupiditate reiciendis. Libero ut iure aut et qui.'),
(108, 65, 75, 'Et saepe et excepturi ea ut iste. Amet molestiae rem qui est. Quia incidunt id consectetur.'),
(109, 47, 68, 'Eius molestiae ut cumque quisquam quibusdam cum. Eos hic praesentium quod expedita animi magnam eveniet. Nulla nostrum impedit quisquam quaerat.'),
(110, 40, 29, 'Eveniet ex consequatur voluptatum quis. Totam esse magni temporibus voluptatem rem inventore.'),
(111, 22, 17, 'In et est quibusdam molestiae officiis. Fugit quibusdam eveniet saepe modi dolores sunt quas labore. Accusantium deserunt ducimus assumenda. Quos et ut ut aut aliquid quo consequatur.'),
(112, 12, 24, 'Itaque quod unde consequatur necessitatibus quisquam. Commodi natus fugit consequatur autem similique. Repudiandae aliquam quis aliquid sint libero.'),
(113, 6, 4, 'Ratione repellat et soluta dolores atque. Dolor doloremque nam voluptatem et aliquid.'),
(114, 67, 76, 'Laudantium qui omnis et et. Labore doloremque ab quisquam fuga autem maiores repudiandae. Ipsum eaque vel optio quis dolor est odit dolores.'),
(115, 57, 86, 'Voluptate quia deserunt quasi quibusdam numquam tempore ad laboriosam. Et rerum aut et quia. Dolor incidunt maxime sed est.'),
(116, 33, 98, 'Delectus aut repudiandae aut voluptas at. Dolor nemo porro dolorem ea voluptate provident. Nostrum perferendis ut voluptatem ea amet est ut.'),
(117, 63, 49, 'Voluptatem ipsa ducimus consequuntur qui asperiores. Aut odio quis accusamus voluptatem qui illo. Dignissimos ratione voluptatem et ut.'),
(118, 18, 86, 'Numquam explicabo cum magnam repudiandae provident asperiores. Quia ducimus sit quia dolores ut.\nEt molestiae nisi consequatur. Tempore provident autem vel ea in vero ipsa minima.'),
(119, 48, 66, 'Vel repellendus voluptatibus molestias voluptas. Fuga quis magnam pariatur explicabo nemo illo officiis consequatur. Magni ut beatae cumque ut doloribus aliquid in.'),
(120, 53, 3, 'Odio ipsam hic et autem sed vitae. Magni at eum dolor nemo culpa. Quia voluptatem dolor esse libero non.');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `pid` bigint(8) NOT NULL,
  `user` bigint(8) NOT NULL,
  `title` varchar(200) COLLATE utf8_bin NOT NULL,
  `content` varchar(20000) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`pid`, `user`, `title`, `content`) VALUES
(1, 99, 'Animi ut occaecati omnis iure magnam aliquam quam.', 'Dolorum beatae porro autem et possimus qui eum. Et facilis soluta quo distinctio. Voluptatem quam quia fugiat quaerat dolore aut. Autem autem aut minus quia optio.'),
(2, 48, 'Nobis laboriosam totam labore aut possimus pariatur recusandae.', 'Accusantium unde dignissimos quia ab quas corporis. Impedit possimus rerum ut ratione qui et a. Illum cum fugit atque.'),
(3, 23, 'Optio consequuntur sint tempore molestiae aut esse.', 'Modi quia eius natus consectetur ab ut. Animi facilis quam placeat. Illo nulla autem ut qui voluptate aut.'),
(4, 79, 'Reprehenderit corporis perferendis culpa voluptatum nisi et quas sint.', 'Animi officia suscipit repudiandae ullam. Molestias reprehenderit quia tempora unde exercitationem voluptatem est tempora. Numquam dicta vel autem possimus iste optio.'),
(5, 11, 'Enim ipsam reprehenderit aspernatur ducimus accusantium.', 'Eaque beatae ipsum repellendus non libero dolore nisi reprehenderit. Et at consequuntur vitae autem nam vitae. Vel dolorum vero voluptas neque est.'),
(6, 69, 'Est et delectus esse.', 'Voluptatem non illo blanditiis molestiae sed qui. Voluptas eos tempora repellat minus quibusdam. In suscipit quia et odio sapiente officia.'),
(7, 77, 'Quia ex voluptas facere non ut.', 'Voluptas neque et omnis voluptate. Iure qui dolor et in ipsa commodi. Rerum sint omnis atque magnam assumenda quia facilis. Eum iste quaerat perferendis.'),
(8, 35, 'Incidunt consectetur a reprehenderit optio accusantium quis velit.', 'Quia voluptatibus atque tempore voluptatem ut. Labore et facilis et nisi minima itaque veniam. Unde aut totam quidem aut et.'),
(9, 34, 'Sequi tenetur recusandae aut a labore.', 'Quidem dignissimos omnis libero quasi quia perspiciatis doloremque. Sed recusandae id nemo necessitatibus sequi. Est blanditiis ut repellendus ipsam dolorum sit. Quaerat sed et nobis nobis aliquam.'),
(10, 50, 'Alias perferendis consectetur qui fugiat et animi.', 'Modi quasi qui aliquam tenetur consequuntur eos quo. Corrupti tempore aut modi et dicta architecto eum eligendi. Iure quaerat quia et illum.'),
(11, 83, 'Sit aut est temporibus nulla quia aut quis.', 'Dolores vel quis enim cum soluta dolores labore. Aut fugiat harum ut laboriosam. Sunt numquam dolore id dolor distinctio facere est ut.'),
(12, 95, 'Aut nam eos praesentium doloremque illum facilis.', 'Ab hic minima perferendis tempora aspernatur. Est consectetur magni qui nihil vel. Voluptas possimus voluptatem incidunt corporis et. Delectus in qui cupiditate quis vel sit.'),
(13, 77, 'Nam vitae aut sequi voluptatum totam maiores fuga commodi.', 'Porro optio sit qui quidem temporibus molestias velit. Provident quas iusto dolorem laboriosam sunt quia. Suscipit iure consectetur molestiae velit enim quas. Neque et saepe quae eos sequi incidunt.'),
(14, 63, 'Enim quia temporibus minima nam et recusandae in.', 'Vero ut et laborum voluptatem porro dolor magnam ea. Praesentium non non sed et eveniet. Eaque sit aperiam sint iste et unde alias. Velit libero quae quisquam eos sint iusto.'),
(15, 15, 'Porro adipisci consequatur cum magnam perferendis.', 'Autem aut quo voluptatum sunt. Perspiciatis dignissimos consectetur nulla iste fugiat. Quia esse deserunt illum eveniet delectus. Sunt reiciendis alias magnam quam exercitationem est.'),
(16, 76, 'Cupiditate quod vel qui nihil autem.', 'Voluptatibus deserunt et rerum odio blanditiis dolor. Temporibus ad commodi cupiditate dolorum magnam quis. Veritatis ut optio et et ut sit. Non voluptatem ut maxime reiciendis qui eius impedit.'),
(17, 90, 'Aut sequi esse minima quaerat dolores repellendus explicabo.', 'Sit alias amet et id deleniti a esse. Magnam facere voluptatem reprehenderit illo alias. Repellendus perferendis facilis suscipit pariatur. Aliquid commodi id corrupti illo maxime in.'),
(18, 31, 'Deleniti et laboriosam perspiciatis eaque sunt quos.', 'Totam repellat fuga reprehenderit incidunt dolorum molestias. Nisi doloribus et eos velit.'),
(19, 47, 'A quia quia ut non.', 'Est sed nihil quisquam laborum ut consequatur vero. Eum facere unde ipsa adipisci atque. Accusamus voluptas consequuntur fugiat ipsum ut rerum quia vitae.'),
(20, 2, 'Aperiam nihil doloribus repellendus est fugiat dolore eum.', 'Voluptatem quia quisquam corporis vel adipisci. Quas quis explicabo illum et. Sed consequatur temporibus reprehenderit rerum sint impedit saepe.'),
(21, 28, 'Quasi debitis rem esse ut quia.', 'Atque consectetur nihil culpa dolorum. Est voluptatem et et vel incidunt rerum expedita.'),
(22, 25, 'Doloremque et ex aliquid et atque veniam.', 'Aperiam eaque modi qui est qui error. Nisi tenetur alias nihil et similique.\nDucimus a ut quibusdam corrupti. Repellat qui fugit vel aut et magni. Sed ullam nihil unde aspernatur aperiam esse.'),
(23, 61, 'Quia rem quisquam aut in minus.', 'Deleniti consequatur qui numquam placeat. Quia et non magnam et possimus et.'),
(24, 57, 'Aut quis quisquam harum magni.', 'Placeat debitis omnis cumque aut ut eos voluptatum. Est praesentium omnis iusto excepturi consequatur.'),
(25, 75, 'Ratione maxime possimus voluptatem magnam.', 'Eos quia et autem et ipsa architecto accusamus. Ullam soluta quia est placeat itaque nihil. Enim voluptates aperiam saepe quo voluptatem placeat est. Modi consequuntur aut et optio.'),
(26, 8, 'Ea laboriosam voluptatem autem beatae.', 'Consequatur et qui ipsa numquam voluptas velit ut. Aut nemo iusto iste et autem reprehenderit. Autem animi et saepe sunt quod aut.'),
(27, 52, 'Eligendi quae maxime illo commodi praesentium repellendus.', 'Officia dignissimos voluptas voluptas quo architecto consequatur. Sunt aut animi molestiae cupiditate. Sint facere enim soluta ipsum. Non deleniti facere neque rerum saepe id blanditiis.'),
(28, 20, 'Accusantium labore reiciendis tempora debitis.', 'Earum dicta voluptatem in a. Eligendi est nulla minima. Asperiores accusamus ad quod dolorum in ut.'),
(29, 43, 'Omnis et et eum libero facere quam.', 'Maxime nesciunt voluptatem molestiae blanditiis natus sint. Harum quia totam amet ducimus. Sequi qui reprehenderit nulla rerum excepturi et.'),
(30, 4, 'In id optio rerum quis perferendis rerum sit.', 'Quo nihil inventore animi alias eius omnis omnis. Sed repellat consequatur sequi doloribus beatae. Earum modi enim corporis odio non officia rerum. Asperiores aspernatur architecto nihil molestias.'),
(31, 77, 'Deleniti dolor ratione ex asperiores ut doloremque.', 'Aperiam necessitatibus id et. Iste qui hic laborum culpa non sunt odit tempora. Fugiat eos ad voluptate eveniet commodi magnam neque voluptas.'),
(32, 15, 'Ut ut assumenda qui.', 'Aspernatur nihil magni earum ab laboriosam. Facere magnam id recusandae soluta esse. Mollitia quisquam modi exercitationem officia voluptatem dignissimos magni.'),
(33, 84, 'Officia est ut dolore totam.', 'Et tenetur itaque omnis quia. Voluptas autem maiores reiciendis quaerat rerum hic itaque aut. Qui aut qui aut.'),
(34, 31, 'Explicabo sint omnis esse nulla et quia sint.', 'Consectetur modi nobis commodi et. Et quas eaque molestiae vero veniam et. Nihil dolore odit ut perferendis. Fuga perspiciatis natus natus quos.'),
(35, 3, 'Eaque aut voluptatibus beatae quia.', 'Consequatur aliquid voluptas veritatis maiores sequi. Asperiores eum autem fuga tempora.'),
(36, 14, 'Temporibus et debitis deleniti doloribus impedit distinctio.', 'Sint dolor hic velit est. Iste excepturi dignissimos voluptatibus ducimus quia exercitationem excepturi. Sit quidem nulla provident consequuntur. Modi officia dolor ut ab et.'),
(37, 74, 'Et nostrum nisi sed.', 'Et consectetur accusantium earum rerum quibusdam est sit. Commodi iusto et explicabo sunt rerum beatae. Dolor odit ut adipisci ipsa.'),
(38, 37, 'Earum nam est quis voluptatem nihil velit cumque.', 'Odio cumque quas iusto modi dolore et. Laudantium itaque optio consectetur temporibus doloribus laboriosam.'),
(39, 99, 'Cum ipsam commodi quibusdam quis voluptas quia temporibus.', 'Tempore eveniet qui aut quo facere qui dicta vel. Debitis necessitatibus reiciendis est ipsa asperiores. Doloremque velit voluptatem tenetur ea.'),
(40, 97, 'Dolor ea iste doloremque rerum.', 'Non ut placeat quasi voluptas. Optio praesentium nostrum voluptatem quae facilis id. Eaque adipisci dolor rerum est itaque cupiditate.'),
(41, 93, 'Laborum qui sunt minima eligendi.', 'Temporibus possimus facilis asperiores. Architecto dolorum odio animi. Corrupti unde blanditiis saepe aut ea omnis veniam ut. Pariatur est saepe est voluptatum quia aliquam.'),
(42, 49, 'Dolor cum voluptatibus alias accusantium eligendi alias.', 'Omnis architecto eum aut repellat porro. Aliquid aliquam ut dolor sed aut veritatis ipsam. Sit pariatur reprehenderit iste ut. Illum alias dolorem aperiam occaecati nemo.'),
(43, 55, 'Repudiandae accusamus quo tempora ullam rerum corporis ut.', 'Accusamus ipsum assumenda vitae nihil. Amet praesentium officia ab neque. Et explicabo harum nulla modi facilis qui architecto beatae.'),
(44, 33, 'Ut quod maxime asperiores accusamus.', 'In sequi eos corporis porro. Accusantium corporis dolor architecto molestiae qui voluptas voluptates voluptatem. Sit eius temporibus amet iure dolorum qui.'),
(45, 26, 'Harum dignissimos aut nobis quia est.', 'Voluptatem eaque aperiam at ut. Et rerum cum consequatur. Pariatur dolorem impedit doloribus velit. Exercitationem quidem quia omnis.'),
(46, 48, 'Aut et id assumenda.', 'Mollitia temporibus ratione quis corporis et quae. Dolores sed necessitatibus exercitationem sit sunt. Et amet consequatur fugit repellat voluptatem.'),
(47, 80, 'Est ipsam temporibus animi sed vero quam qui sapiente.', 'Maxime praesentium iste et. Quae sunt laudantium placeat eum ea.'),
(48, 74, 'Neque incidunt quidem voluptas quia nam ut animi.', 'Maxime iste harum modi. Quaerat quam aspernatur magni vero non. Quia itaque non quis distinctio aut cumque dolorem. Reiciendis non qui non hic. Eveniet quia dolorum molestiae eum.'),
(49, 76, 'Aut eaque soluta aperiam perferendis sint.', 'Qui nulla quibusdam iusto. Mollitia modi fuga quo est voluptates dolores commodi. Quod tenetur at sint et quasi repellendus.'),
(50, 98, 'Reiciendis et incidunt hic recusandae hic libero.', 'Et doloremque commodi laudantium iusto sed earum autem quos. Dolorum porro fuga saepe tempora itaque aliquid. Minus est est soluta dolore omnis.'),
(51, 66, 'Ea incidunt facere tempore.', 'Nobis occaecati consequatur perferendis sit impedit sed. Non atque esse optio consequatur officia. Est enim consequatur accusantium ratione temporibus. Sit et rem repellat aut illum dolore.'),
(52, 78, 'Accusantium voluptatem provident accusantium dolores.', 'Dolor perferendis et eos placeat sit ut molestiae. Provident quaerat non voluptate quibusdam corporis reprehenderit eligendi. Quas aut qui quos at. Corrupti nisi officia est laboriosam autem veniam.'),
(53, 50, 'Ipsam optio quasi earum.', 'Laboriosam sed modi sequi. Excepturi aut ea nisi fuga voluptatem. Assumenda quisquam quas cumque consectetur autem tenetur. Blanditiis et quaerat rerum quam et.'),
(54, 34, 'Officia quo quia culpa.', 'Dolor error laboriosam esse culpa. Reprehenderit placeat velit aliquam repudiandae sequi corrupti soluta.'),
(55, 62, 'Sit asperiores suscipit autem qui minima.', 'Quasi omnis voluptas inventore ut sunt sint. Et sit excepturi doloribus nemo id dignissimos veritatis est. Velit eum sunt vel. Ex aut fugit debitis debitis.'),
(56, 95, 'Distinctio ad enim qui.', 'Nisi ipsam in molestiae. Cum magnam est facere explicabo cumque et perferendis quae. Porro omnis consequatur deserunt.'),
(57, 42, 'Atque expedita atque odit quo qui.', 'Tempora quae ut eaque autem. Sit nisi a minima. Ipsam fugiat aliquid sequi ut. Amet aliquam voluptate doloremque laboriosam.'),
(58, 89, 'Voluptatem assumenda similique eveniet similique voluptate.', 'Omnis sit reiciendis officia natus enim quia libero. Qui exercitationem hic qui earum nostrum. Odit sed est laudantium beatae.'),
(59, 33, 'Et magnam quidem dolorem beatae.', 'Fugit totam enim delectus culpa accusamus. Sed quis quia harum vel natus.'),
(60, 61, 'Quae ipsam dolorem est ea et.', 'Officiis inventore ex laborum officiis. Ut magni ducimus nam dolorum explicabo rem. Qui blanditiis incidunt impedit esse quo dolor ab. Vero inventore voluptas qui ut.'),
(61, 66, 'Et libero et voluptates ad.', 'In repudiandae blanditiis minima odio rerum. Libero voluptatum sit et illum dolor placeat. Rerum deserunt praesentium ut et aut dignissimos.'),
(62, 6, 'Odit dolore ea quis non et consequatur.', 'Explicabo asperiores quidem velit unde tempore. Corporis hic iste pariatur corporis quo. Ut sit voluptate unde aut reprehenderit dolorum molestias perferendis. Atque vero fuga sunt ipsam.'),
(63, 82, 'Sapiente adipisci deserunt magnam veritatis nulla.', 'Modi magnam quia nemo praesentium quis. Dolores hic voluptate vero. Sint et mollitia aut at aut.\nIllum ut provident ullam error animi. Omnis quia vel ut. Vitae temporibus aut aut fugit et qui.'),
(64, 38, 'Non eligendi est tempora soluta et aut accusantium molestiae.', 'Veniam nulla temporibus at molestiae. Optio delectus voluptatem similique eaque id maxime expedita. Molestias ratione vel et.'),
(65, 37, 'Modi earum laboriosam sit doloribus ipsa corrupti eius.', 'Tempore dolor et a eaque reiciendis blanditiis natus nobis. Et pariatur omnis incidunt magnam. Quos in nisi dolorum. Tempore et amet rerum.'),
(66, 58, 'Eius quae saepe qui consequatur fuga.', 'Sequi laboriosam minus rerum. Incidunt est sequi nesciunt saepe. Et molestias sunt magni placeat.'),
(67, 49, 'Adipisci nam quis ad vero.', 'Modi qui debitis voluptas est consequatur dolorem qui. Voluptates omnis iusto culpa aperiam quaerat. Esse sit expedita omnis et ut rem nihil eum.'),
(68, 68, 'Soluta voluptate quia ducimus et aut vero omnis.', 'Qui fuga mollitia possimus ipsum. Sed labore iste nam expedita quam eaque quo. Quae qui et quo.'),
(69, 91, 'Recusandae in sint doloribus autem recusandae aut.', 'Fuga dignissimos qui exercitationem. Numquam consequuntur minus hic exercitationem voluptatibus mollitia. Aspernatur atque incidunt alias. Consequatur maxime quia voluptatem quibusdam vero.'),
(70, 81, 'Dolorem eum animi numquam architecto at voluptatum dolore optio.', 'Qui tempore qui libero autem nulla. Dolores ea ducimus et officia fuga assumenda. Quisquam nostrum aut labore repellat. Voluptas numquam iusto et aut recusandae dignissimos.');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `uid` bigint(8) NOT NULL,
  `email` varchar(128) COLLATE utf8_bin NOT NULL,
  `password` varchar(128) COLLATE utf8_bin NOT NULL,
  `userType` enum('admin','moderator','user') COLLATE utf8_bin NOT NULL DEFAULT 'user'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`uid`, `email`, `password`, `userType`) VALUES
(1, 'bartell.martine@example.com', '40bcc6f6193986153cae1bb1c36668650a3d5f97', 'admin'),
(2, 'zcrona@example.net', '1f66d81577cd95514cedc8504d65ec8eff9c336a', 'moderator'),
(3, 'wgaylord@example.com', '3fcba21eebd2d09681515b4849d2bbeae566451e', 'user'),
(4, 'jturcotte@example.net', '933045bb8d5e3a822514cdfc1ec357251adffdcb', 'admin'),
(5, 'rhiannon66@example.org', 'a838c262bc3c9ad86b74d76e603521acd527ceec', 'moderator'),
(6, 'ansley.schaden@example.org', '9f42f34c0260c3ce60e17bc0852159c7d95d43ff', 'user'),
(7, 'arne.huel@example.org', '597b70e4637cfedd8e79b7c70709a61287083e00', 'admin'),
(8, 'cmcclure@example.org', '9434525ad74f9f4e59f7a4f212ce223c19ef0af4', 'admin'),
(9, 'felipa35@example.org', '7fa816135c494632dc0b979574864f358f617e0e', 'user'),
(10, 'bfeest@example.org', '29c2651c84fc7d360900ecd0ff95bbaaec075241', 'admin'),
(11, 'josie76@example.net', 'a13ea462dbcd7dd3b3b97b54620f46df98bbd86c', 'admin'),
(12, 'kaylie.kreiger@example.org', '1aecb3924d2470c0a5225ebb4bc2816a4d94167e', 'user'),
(13, 'cara.simonis@example.net', '1392d07010e6b1cd0930342965c3c575b1613f4a', 'admin'),
(14, 'mcole@example.org', '1ed090707e27ee9a66da3b06445c3d3c3294a10c', 'user'),
(15, 'hane.jerrold@example.net', '50ada3ddd15eaaeaa8c5f7fe7b1403150e6ebbcf', 'moderator'),
(16, 'ubaldo26@example.com', '8f479dac76b07ea8cd8e52389d78f455b9b7956e', 'user'),
(17, 'mckayla44@example.net', '54364b44cb3ef9e8ce2412ba0870bb14a6657c78', 'user'),
(18, 'lueilwitz.ashton@example.com', '1960bbb47229c727d1ec814fb932d48372fee1cf', 'moderator'),
(19, 'lesley.funk@example.net', '7d89b525044787dd0767b2c24b91da2b66098ee8', 'admin'),
(20, 'samir.kihn@example.net', '533cdfb77c5126023897d43b1cdc32444ebe5559', 'moderator'),
(21, 'stroman.davin@example.com', '37615c8b741a61931826e818512465fe204b9b9c', 'moderator'),
(22, 'kylie.lowe@example.org', '485fd3ba2fcdf90a76e7e05f84c1353618f32b8f', 'user'),
(23, 'loma75@example.org', '846b84fa1de6bf2b1aec1deb83bbb8a503d508e5', 'user'),
(24, 'rodriguez.emely@example.org', 'e317596948ccd18f7bdf44ddd62e23a1a139d4dd', 'user'),
(25, 'aufderhar.rudy@example.com', '78050e414e4d859ac40a04e47293be5fe8836180', 'admin'),
(26, 'kaela57@example.net', 'a15e74b7a943fb1e0973ae488bd4724aaa1560de', 'user'),
(27, 'prosacco.maximilian@example.com', 'ca50850ec75c07b6c5659d584b0152d654b868cc', 'user'),
(28, 'lyda92@example.net', '1082609051a58d2efd71ca4ea535411eef1443d6', 'moderator'),
(29, 'kayla06@example.net', '741ffc8d5e6e8bf80560f7099b36748aecc107bc', 'user'),
(30, 'sam44@example.net', 'f749aadbf5d1d1fd0b1ec46befd8a8e7b1260960', 'user'),
(31, 'ryann.kertzmann@example.com', 'e4ca9c6cf7255aea55a722bdb2e3097a959c653d', 'moderator'),
(32, 'ceasar.reichel@example.net', 'd6c022b55ea529e5d2c1bd5eb0ce2b1afdfb3fb3', 'user'),
(33, 'alyce22@example.net', '12b3c6a9e479dcd64f888000bffac75b07d7c1d7', 'user'),
(34, 'oglover@example.net', 'df361bfb216355fff061f9e73560d9838b75874e', 'user'),
(35, 'bcarroll@example.com', 'c73324e12880a036059ef8add4ba19c6a48f4869', 'moderator'),
(36, 'hkeebler@example.org', 'd580160bfe2e5e1840e11e8fa1e9d0b497cf2d76', 'moderator'),
(37, 'ggusikowski@example.net', '23b818b9d9a4cf6f97ed974c57582f8ea46b13a4', 'admin'),
(38, 'stuart62@example.org', '930059dc0d7c29510fec65c0291a396e32e36e03', 'moderator'),
(39, 'zena31@example.net', 'b40e86d5deb2bd4a2d10b7313c5e11d0c55cca8e', 'admin'),
(40, 'lokeefe@example.com', 'b0198d090f91be1413c648e30c17dc4ada756d51', 'user'),
(41, 'ward.conor@example.net', 'e52ab6c7d53ab2ce261dd5e66b70addb3f711df6', 'admin'),
(42, 'wilhelm36@example.net', '4fbacdefe98ba3f65e612faef04b4a2b72f2d968', 'admin'),
(43, 'teresa.friesen@example.com', '64e6e4810a305ce4e6f162debfb6807571778ba6', 'moderator'),
(44, 'timothy64@example.net', '5e67a7fa1d58ecd938afe6b4e63c30b4073d11e1', 'admin'),
(45, 'arvel.batz@example.org', '39c9cfad572e4a6e56d358fba096ea90d71943a7', 'user'),
(46, 'terdman@example.org', '4f68429e9f5e0496f191b25064fb90197ffcb06c', 'moderator'),
(47, 'prosacco.rachelle@example.net', 'da031be03d685bc6e53076649dab3705c57badc7', 'admin'),
(48, 'nicholaus68@example.net', '7db960fbb24cfb2d23b9046ba23771077e8e7e36', 'moderator'),
(49, 'lambert.bartell@example.net', '18e00f5fdec007ed1439fd3ff0e46c751cf83737', 'moderator'),
(50, 'connelly.rolando@example.net', 'f0582a7c4520a26ae8c8e2a8af93684e868a8a09', 'moderator'),
(51, 'elliot.watsica@example.org', '750fd09faebc6e3aba3de7246489033d13bb8784', 'user'),
(52, 'casper.padberg@example.org', '5df117d8568cd1c7b616bcc64ac2310cf5f1e881', 'admin'),
(53, 'francisco.keebler@example.com', 'ab26026a8802e8b057e8cfb9db95a282a4afdcb8', 'admin'),
(54, 'lynch.curtis@example.net', '59e930ea45e33da64c74b47353aa45ad00ab39c8', 'moderator'),
(55, 'purdy.retha@example.org', '27d65132f320194b274c57a629a6f0e5f8d98ab3', 'admin'),
(56, 'white.colten@example.net', '08897f1d2e969f8234aa58bf79d2bb6757fa8c1f', 'moderator'),
(57, 'mollie41@example.net', 'c837e0eb5645217d091a4c6a048d73ee2f6ee79e', 'moderator'),
(58, 'madaline57@example.org', '9dfaa92a62c5d87f1ef0939c3c8c7da6af41b04c', 'user'),
(59, 'chloe35@example.net', '1c6ecc66760be64d412159adf7870a7d67dcc030', 'moderator'),
(60, 'nayeli18@example.net', 'f888da01484f0b1bc6066e704b6fd79951467fb6', 'admin'),
(61, 'fblock@example.com', '4137598346e3494132de7f4fb4baf44363459605', 'admin'),
(62, 'reichert.tyrell@example.com', '84e280050fe703623c61de1569ca78e6de4a4538', 'moderator'),
(63, 'rbeer@example.org', '48279885764cf03b6d572df284ae0cd1a11c2f91', 'moderator'),
(64, 'emory16@example.org', 'f1e9e179514317c41665f3c36a49d0241700afb1', 'user'),
(65, 'lisa46@example.org', 'bae4c2c441ebf2e0aa782e8277369778017e68f6', 'admin'),
(66, 'pgutkowski@example.net', '2de02919349c8692078058565165ddcf4e771745', 'moderator'),
(67, 'leta.nolan@example.org', '26b5011b4dbe91f77029b9a20faa37fa7612eeb5', 'user'),
(68, 'oyundt@example.net', '8f51eb68b7e5b2a22fcca98039bcfc355ded599a', 'moderator'),
(69, 'alexa96@example.org', '16a89b58959d657927fc9d903bd3861e19a65d66', 'moderator'),
(70, 'gerhold.lazaro@example.net', '0574a3dd8e5dc60f4baff64e41e3940c4585c59b', 'user'),
(71, 'cremin.dena@example.com', 'b8cbd24d839fec336b98835df1fc2e21b2b54b55', 'user'),
(72, 'carmela05@example.com', '869953170cd94a7c610c0b18737759f376460bfb', 'moderator'),
(73, 'heaney.caleb@example.net', 'c702da0ce5c442f6d6d8fc2d2b6351179ed05821', 'admin'),
(74, 'barrows.alexandria@example.com', '0d418f1acf8afef37c3fbe5387a9e344e4cebf1a', 'admin'),
(75, 'mzboncak@example.org', '47ab423b2dee193192c78813bce9e2a436bcbe42', 'moderator'),
(76, 'vsatterfield@example.com', 'e6ef3745369fe17bc9aa79fbbeaae6b9c0506e78', 'user'),
(77, 'mberge@example.com', '76b1db0a56c2b6a510292e7dce534479cbaccc9d', 'admin'),
(78, 'finn41@example.com', '04450a4e59323517d8665e2c9ec73aed26679be3', 'moderator'),
(79, 'ykuhlman@example.net', 'fe1a15b9226dca7318a2824eb925c93f9266ba05', 'moderator'),
(80, 'kira.powlowski@example.org', '8ba54bf0d8f7046610cfe15673227c5a53c9825f', 'moderator'),
(81, 'nstamm@example.org', 'd16dbd5be3897dbcf233a45e6dc0334c0688f763', 'moderator'),
(82, 'wrobel@example.org', '05bffc2aa6bd75b26aac649416f4f0dda48dc869', 'admin'),
(83, 'rosella.brekke@example.com', 'b32aa64965468f64509f7dd8163e9419a095e9a2', 'moderator'),
(84, 'kwolf@example.com', '1028114516ea54ab09bcf106fd3bad2b25c8f2e1', 'user'),
(85, 'vwintheiser@example.org', '56d38135295072696295db3e38718d4b0cf403e6', 'moderator'),
(86, 'rutherford.ola@example.net', '92c62918371383655df58524bdce62221ef7c20c', 'admin'),
(87, 'pinkie.leffler@example.net', 'a16fb2671ab7d0fbc2cc1d27bc8acc05113aa23f', 'admin'),
(88, 'jovany.ebert@example.com', '37442ead1ea7861ad47d44036294f446340b56fe', 'user'),
(89, 'icremin@example.com', 'ed3922daee839091c41414bf825f38ea33e9baca', 'moderator'),
(90, 'huel.jonatan@example.net', '7a4eaa1c22655fd45e2ec8bacc47b9c560963206', 'moderator'),
(91, 'francisco71@example.org', '9253602e127aeff50a87ccb3bf67db994682ad26', 'admin'),
(92, 'marquise84@example.net', 'c7dd8fb44f332342f3b7d94623c9c4f4c4477d7a', 'user'),
(93, 'abbott.jaylin@example.com', 'bc7c1d9b644e76ef7117b6a8f2df62112466f4c6', 'admin'),
(94, 'runolfsson.percival@example.org', '5cb6c594493943d68003b6962d86f47a50f30a9f', 'user'),
(95, 'fay.marlen@example.org', '84928c27681a2ef10fca94d1be53e1381d8cff48', 'moderator'),
(96, 'greenfelder.tyrell@example.org', '6022a3459e857d3bb4654e1bd66d53bc7c270857', 'admin'),
(97, 'justice.wisoky@example.com', '8d663081bb8465e7d1d328a4d385df5611315340', 'moderator'),
(98, 'murray.karelle@example.net', '4565bcdafd8bfd6e1d576c085066fa259f06f435', 'admin'),
(99, 'logan07@example.org', '6701124a9562a07a74a4c615d0436459fb167378', 'user'),
(100, 'schiller.glenna@example.net', '446ac41a5b9bfb475854332e3f3e328eaa344728', 'moderator');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`cid`),
  ADD KEY `user` (`user`),
  ADD KEY `post` (`post`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`pid`),
  ADD KEY `user` (`user`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`uid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `cid` bigint(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=122;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `pid` bigint(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `uid` bigint(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`user`) REFERENCES `users` (`uid`),
  ADD CONSTRAINT `comments_ibfk_2` FOREIGN KEY (`post`) REFERENCES `posts` (`pid`);

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_ibfk_1` FOREIGN KEY (`user`) REFERENCES `users` (`uid`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
